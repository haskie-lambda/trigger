Trigger 
------- 
An extension to create keyboard triggers running custom JavaScript on 
configured pages for Chromium based browsers. 

# How it works 
This extension enables you to insert custom JS actions on webpages that 
match a specified URL. These actions have an associated keyboard shortcut 
which when triggered will run the JavaScript code. 
It is also possible to insert JS code without associating a shortcut in 
which case it is only run on `document.onReady`. 

# How to configure 
Go to the settings page of the extension and inspect the already installed 
listeners for Youtube Music to get started. 

# Installation 
- Either download the ZIP file or clone the repository 
- Go to your extension settings (`chromium://extensions`) 
- Enable developer mode 
- If you downloaded the ZIP file, unzip it into a directory of your choice. 
  (e.g. `HOME/extensions/trigger/[the extracted files]`) 
- Click on `Load unpacked` and select the folder. 

# Contibutions 
Contributions welcome :) 

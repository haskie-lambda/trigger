const styles= ".trigger_question {"
            + "  position:absolute;"
            + "  z-index:9999999;"
            + "  display:flex;"
            + "  flex-direction:row;"
            + "  margin-left: auto;"
            + "  margin-right: auto;"
            + "  left: 0;"
            + "  right: 0;"
            + "  align-items: baseline;"
            + "  height:30px;"
            + "  width:90%;"
            + "  top:20px;"
            + "  background:lightgray;"
            + "  border-radius:5px;"
            + "  box-shadow: 3px 3px 3px 3px;"
            + "}"
            + ".trigger_question p {"
            + "  font-size: 1.25em;"
            + "  margin-right: 5px;"
            + "  margin-left: 5px;"
            + "  margin-top:auto;"
            + "  margin-bottom:auto;"
            + "}"
            + ""
            + ".trigger_question input {"
            + "  flex-grow: 1;"
            + "  margin-top: 2px;"
            + "  margin-bottom: 2px;"
            + "  margin-right: 5px;"
            + "  font-size: 13pt;"
            + "}"

var styleSheet = document.createElement("style")
styleSheet.type = "text/css"
styleSheet.innerText = styles
document.head.appendChild(styleSheet)


function ask(q) {
  let main=document.createElement("div");
  main.classList.add("trigger_question");
  main.id="trigger_question"
  let input=document.createElement("input");
  input.id="answer";
  let question=document.createElement("p");
  question.textContent=q;
  main.appendChild(question);
  main.appendChild(input);
  document.getElementsByTagName("body").item(0).prepend(ask("hi"));
  document.getElementById("trigger_question").focus();

  return main;
}

function main(){
  chrome.storage.local.get(['config'], function(config) {
    console.log("config", config);

    $(document).ready(function() {

      config.config.triggers.forEach(trigger => {
        if(document.URL.match(trigger.url)){
          console.log("registering trigger", trigger);
          document.addEventListener(trigger.listener, (e) => {
            if(e.code === trigger.key
              && ((trigger.modifier && e.modifier === trigger.modifier) 
                  || !trigger.modifier && !e.modifier)) {
              eval(trigger.method);
            }
          });
        }
      });
    });
  });
}

chrome.storage.local.get(['config'], function(config) {
  console.log("config", config);
  if(!config || !config.config || !Array.isArray(config.config.triggers)){
    console.log("setting defaults");
    chrome.storage.local.set({ 'config': {'triggers':
      [ { "url": "music.youtube.com"
        , "listener": "keyup"
        , "key": "Backspace"
        , "modifier": null
        , "method": 'document.getElementsByClassName("next-button")[0].click();'
        }
      , { "url": "music.youtube.com"
        , "listener": "keyup"
        , "key": "Delete"
        , "modifier": null
        , "method": 'document.getElementsByClassName("previous-button")[0].click();'
        }
      , { "url": "digital-independence.xyz"
        , "listener": "keyup"
        , "key": "Backspace"
        , "modifier": null
        , "method": 'alert ("backspace pressed");'
        }
      ]
    } }, () => { console.log("done setting defaults"); main(); });
  } else {
    main();
  }
});



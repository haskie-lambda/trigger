var counter = 0;
chrome.storage.local.get(['config'], function(config) {
  console.log("config", config);
  if(!config || !config.config || !Array.isArray(config.config.triggers)){
    console.log("setting defaults");
    chrome.storage.local.set({ 'config': {'triggers':
      [ { "url": "music.youtube.com"
        , "listener": "keyup"
        , "key": "Backspace"
        , "modifier": null
        , "method": 'document.getElementsByClassName("next-button")[0].click();'
        }
      , { "url": "music.youtube.com"
        , "listener": "keyup"
        , "key": "Delete"
        , "modifier": null
        , "method": 'document.getElementsByClassName("previous-button")[0].click();'
        }
      , { "url": "digital-independence.xyz"
        , "listener": "keyup"
        , "key": "Backspace"
        , "modifier": null
        , "method": 'alert ("backspace pressed");'
        }
      ]
    } }, () => { console.log("done setting defaults"); main(); });
  } else {
    main();
  }
});

function getRow(rowid, trigger){
  let row = document.createElement("div");
  row.id = rowid;
  row.classList.add("myrow");
  let url = document.createElement("input");
  url.value = trigger.url;
  url.classList.add("url");
  url.classList.add("field");
  url.placeholder="URL";
  let listener = document.createElement("input");
  listener.value = trigger.listener;
  listener.classList.add("listener");
  listener.classList.add("field");
  listener.placeholder="When";
  let key = document.createElement("input");
  key.value = trigger.key;
  key.classList.add("class");
  key.classList.add("field");
  key.placeholder="Key Code";
  let modifier = document.createElement("input");
  modifier.value = trigger.modifier;
  modifier.classList.add("modifier");
  modifier.classList.add("field");
  modifier.placeholder="Modifier";
  let method = document.createElement("textarea");
  method.value = trigger.method;
  method.classList.add("field");
  method.classList.add("method");
  method.placeholder="JavaScript to execute: alert('Hello World!')";
  let btnRemove = document.createElement("button");
  btnRemove.textContent = "Delete";
  btnRemove.classList.add("deleteButton");
  btnRemove.onclick = (e) => {
    row.parentNode.removeChild(row);
      chrome.storage.local.get(['config'], function(config) {
        config.config.triggers.splice(rowid,1);
        chrome.storage.local.set(config,()=>{
          location.reload();
        });
      });
  };
  let firstRow = document.createElement("div");
  firstRow.classList.add("rowrow");
  firstRow.appendChild(url);
  firstRow.appendChild(listener);
  firstRow.appendChild(key);
  firstRow.appendChild(modifier);
  row.appendChild(firstRow);
  let secondRow = document.createElement("div");
  secondRow.classList.add("rowrow");
  secondRow.appendChild(method);
  row.appendChild(secondRow);
  let thirdRow = document.createElement("div");
  thirdRow.classList.add("rowrow");
  thirdRow.appendChild(btnRemove);
  row.appendChild(thirdRow);
  return row;
}

$(document).ready(()=>{
  chrome.storage.local.get(['config'],function(config){
    console.log(config);
    config.config.triggers.forEach(trigger => {
      document.getElementById("table").appendChild(
        getRow(counter++, trigger)
      );
    });
    $(document).on("change", ".myrow", function (e) {
      let id = e.target.parentNode.parentNode.id;
      let stuff = e.target.parentNode.parentNode.getElementsByClassName("field");
      
      chrome.storage.local.get(['config'], function(config) {
        config.config.triggers[id].url = stuff[0].value;
        config.config.triggers[id].listener = stuff[1].value;
        config.config.triggers[id].key = stuff[2].value;
        config.config.triggers[id].modifier = stuff[3].value;
        config.config.triggers[id].method = stuff[4].value;
        chrome.storage.local.set(config,()=>{});
      });
    });
  });
  document.getElementById("getConfig").onclick = function(e) {
    chrome.storage.local.get(['config'], function(config) {
      let octets=btoa(JSON.stringify(config));
      window.location.href="data:application/octet-stream;charset=utf-16le;base64," + octets;
    });
  };
  document.getElementById("restoreConfig").onclick = function(e) {
    let inp = document.createElement("input");
    inp.type="file";
    inp.addEventListener('change', (event) => {
      let file=event.target.files[0];
      console.log(file);
      const reader = new FileReader();
      reader.addEventListener('load', (event) => {
        chrome.storage.local.set(JSON.parse(event.target.result),()=>{
          location.reload();
        });
      });
      reader.readAsText(file);
    });
    inp.click();
  };

  document.getElementById("addBtn").onclick = function(e) {
    let nothing = 
      { "url":null
      , "listener":null
      , "key":null
      , "modifier":null
      , "method":null
      };

    document.getElementById("table").appendChild(
      getRow(counter++,nothing)
    );
    
    chrome.storage.local.get(['config'], function(config) {
      config.config.triggers.push(nothing);
      chrome.storage.local.set(config,()=>{});
    });
  };
  document.getElementById("saveBtn").onclick = function(e){
    for(var i=0; i<document.getElementsByClassName("myrow").length; i++){
      $(document.getElementById(i).childNodes[0].childNodes[0]).trigger("change");
    }
  };
});
